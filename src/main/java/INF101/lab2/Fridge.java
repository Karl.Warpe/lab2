package INF101.lab2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;


public class Fridge implements IFridge{

	private FridgeItem[] items = new FridgeItem[20]; 

	@Override
	public int nItemsInFridge() {
		int n = 0; 
		for (int i = 0; i < items.length; i++){
			if (items[i] != null){
				n++;
			}
		}
		return n;
	}

	@Override
	public int totalSize() {
		return items.length;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		int n = nItemsInFridge(); 
		if (n < 19){
			items[n+1] = item; 
			return true; 
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		
	}

	@Override
	public void emptyFridge() {
		items = new FridgeItem[20]; 
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		return null;
	}

	public static void main(String[] args) {
		IFridge fridge = new Fridge(); 
		fridge.nItemsInFridge(); 
	}

}
